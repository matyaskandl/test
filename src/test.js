const Immutable = require('immutable')
const assert = require('assert')

/**
 * Serialize nested errors to string containing unique errors concatenated by "."
 *
 * @param {Immutable.List | Immutable.Map} errors
 * @param {string} [prevErrorString='']
 * @returns string
 */
function toErrorString(errors, prevErrorString = '') {
  return errors.reduce((errorString, error) => {
    if (typeof error === 'string') {
      if (errorString.includes(error)) {
        return errorString
      }

      return `${errorString && errorString + ' '}${error}.`
    }

    return toErrorString(Immutable.fromJS(error), errorString)
  }, prevErrorString)
}

/**
 * Serialize nested errors to strings and preserve their nested structure
 *
 * @param {Immutable.List | Immutable.Map} errors
 * @returns Immutable.List | Immutable.Map
 */
function toStructuredErrors(errors) {
  return errors.reduce((structuredErrors, error, indexOrKey) => {
    if (typeof error === 'string') {
      return `${
        typeof structuredErrors === 'string' ? structuredErrors + ' ' : ''
      }${error}.`
    }

    return structuredErrors.set(
      indexOrKey,
      toStructuredErrors(Immutable.fromJS(error))
    )
  }, errors.clear())
}

/**
 * Serialize nested errors to strings and preserve nested structure for defined keys
 *
 * @param {Immutable.Map} errors
 * @param {string[]} preserveStructureForKeys
 * @returns Immutable.Map
 */
function toFlatErrors(errors, preserveStructureForKeys) {
  return errors.map((nestedErrors, key) =>
    preserveStructureForKeys &&
    preserveStructureForKeys.some(_key => _key === key)
      ? toStructuredErrors(Immutable.fromJS(nestedErrors))
      : toErrorString(Immutable.fromJS(nestedErrors))
  )
}

it('should tranform errors', () => {
  // example error object returned from API converted to Immutable.Map
  const errors = Immutable.fromJS({
    name: ['This field is required'],

    age: ['This field is required', 'Only numeric characters are allowed'],
    urls: [
      {},
      {},
      {
        site: {
          code: ['This site code is invalid'],
          id: ['Unsupported id'],
        },
      },
    ],
    url: {
      site: {
        code: ['This site code is invalid'],
        id: ['Unsupported id'],
      },
    },
    tags: [
      {},
      {
        non_field_errors: ['Only alphanumeric characters are allowed'],
        another_error: ['Only alphanumeric characters are allowed'],
        third_error: ['Third error'],
      },
      {},
      {
        non_field_errors: [
          'Minumum length of 10 characters is required',
          'Only alphanumeric characters are allowed',
        ],
      },
    ],
    tag: {
      nested: {
        non_field_errors: ['Only alphanumeric characters are allowed'],
      },
    },
  })

  // in this specific case,
  // errors for `url` and `urls` keys should be nested
  // see expected object below
  const result = toFlatErrors(errors, ['url', 'urls'])

  assert.deepEqual(result.toJS(), {
    name: 'This field is required.',
    age: 'This field is required. Only numeric characters are allowed.',
    urls: [
      {},
      {},
      {
        site: {
          code: 'This site code is invalid.',
          id: 'Unsupported id.',
        },
      },
    ],
    url: {
      site: {
        code: 'This site code is invalid.',
        id: 'Unsupported id.',
      },
    },
    tags:
      'Only alphanumeric characters are allowed. Third error. ' +
      'Minumum length of 10 characters is required.',
    tag: 'Only alphanumeric characters are allowed.',
  })
})
